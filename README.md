# SK infosec 클라우드 AI 전문가 양성과정

## WEEK 02
### 07/14
* readme.md 마크다운 첫 시작
* 마크다운은 jupyter의 마크다운 사용하면 편하당

## docker 설치 및 운영 계획
* image는 issue에 클립보드 경로 복붙하고 링크 받아와서 걸어둔다

## docker 특징
    * vmware가 실행되어야 docker가 실행된다.
    * 근데 docker는 하이퍼v가 필요하고 vmware는 필요하지 않다.
    * 그래서 toolbox쓰는 것이다
    * docker는 리눅스 환경에서 쓰는게 좋다. window10에서는 지원해준다. 그래서 vmware가 필요없다.
### VM WARE
    * 그러나 대부분의 window10은 container가 필요하다.
    * 리눅스 명령어는 virture box에서 간단하게 쓰면 좋다.
    * point는 모든 기반은 unix이다.
    * right control은 마우스 오른쪽 + 아래
    * pwd 검색시 -> /home/docker


## 기술사 자격증 취득하기
    * 기술사가 있으면 경력으로 인정해서 연봉 책정된다.
    * 집중하면 따기 쉽다. 볼 수 있는 자격이 있다.

# docker 수업 1장 시스템과 인프라 기초 지식

### 시스템과 인프라 기초 지식
1. 서버
    * cpu
    * 메모리
    * 스토리지
2. 네트워크 주소
    * mac주소
    * ip주소 (IPv4를 많이 씀, 최근 IPv6 사용)
    [한국인터넷진흥원자료실](https://www.kisa.or.kr/public/laws/laws3.jsp)
### 하드웨어와 네트워크 기초 지식
1. OSI 참조 모델가 통신 프로토콜
    * tcp/ip 프로토콜(https://m.blog.naver.com/PostView.nhn?blogId=wnrjsxo&logNo=221184538679&proxyReferer=https:%2F%2Fwww.google.com%2F)


2. 방화벽
    * 보안 확보하기 위해 불필요한 통신 차단
3. 라우터/레이어 3스위치
    * 2개 이상의 서로 다른 네트워크 간을 중계하기 우한 통신 장비

### OS 기초 지식
* 리눅스 기초
    * SHELL : 리눅스 커넬 관리
        -사용자가 명령을 내려서 수행을 하기 위해서는 COMMAND가 필요한데, 해당 COMMAND의 모음을 SHELL이라고 한다.
    * is-al치면 위 제품이 나온다.
    * 리눅스 용어의 신기함...다경언니 전공분야이다.bbbbb크~bbbbb
    * 
### 인프라 구성관리 기초 지식
* now infra is coding!!

    * 클라우드는 코딩을 통해 인프라를 구축한다.
    * 자도화 툴이 있기 때문에 이를 이용하여 가능하다.
    * kubernetes로 할 수 있다.

    1. 지속적 인티그레이션 : CI/CD
        * 애플리케이션의 코드를 추가/수정할때마다 테스트를 실행하고 확실하게 작동하는 코드를 유지하는 방법
        * DevOps dev가 coding을 통해 pipeline을 잘 해줘야한다.

# docker 수업 2장 컨테이너 기술과 Docker의 개요

### 컨테이너 기술의 개요
### docker의 개요
* 모두 동일한 환경이 아닐 경우 힘들게 된다.
* 따라서 docker에 환경 세팅해서 해당 환경을 docker에 올린다.
* 이미지는 변경 불가능하다. -> immutable
    * 이미지가 바뀌면 이전 이미지 삭제하고 다시 올라간다.
    * 따라서 일정 시정 쓰지 않는 이미지는 날리는 작업이 필요하다
### docker의 기능
1. build
    * __docker 이미지를 만드는 기술__
    * docker는 애플리케이션의 실행에 필요한 프로그램 본체, 라이브러리, 미들웨어, os나 네트워크 설정 등을 하나로 모아서 docker이미지를 만듦
    * 도커 허브에서 한다
2. ship
    * __docker 이미지를 공유하는 기술__
    * docker 이미지는 docker 레지스트리에서 공유 가능
3. run
    * __dockr 이미지를 작동시키는 기능__
    * dockr engine을 통해서 하게 될 것이다.

### docker의 작동 구조
1. namespace
* __namespace__
* 리눅스 기술의 기본 기술 중 하나.
* 이런 공간을 활용해 PID namespace, Network namespace, UID namespace, 등등의 기술 활용 -> 쿠버네틱스를 통해 사용됨

2. 릴리스 관리 장치(cgroups)
    * 프로세스와 스레드를 그룹화하여, 그 그룹 안에 존재하는 프로세스와 스레드에 대한 관리를 수행하기 위한 기능 계층 구조를 사용하여 프로세스를 그룹화하여 관리 가능. 

3. 네트워크 구성(가상 브리지/가상 NIC)
    * Linux는 Docker를 설치하면 서버의 물리 NIC가 docker0이라는 가상 브리지 네트워크로 연결. 

# 제 3장 Docker 설치와 튜토리얼
* ''' docker run hello-world'''

=======
* ``` docker run hello-world```

## DOCKER 명령
### nginx 환경 구축
* nginx는 가장 많이 사용하는 웹 서버
1. nginx 설치
    *  도커허브에 검색 후 설치
        * 방법 1
        * ``` $ docker run --name some-nginx -v /some/content:/usr/share/nginx/html:ro -d nginx ```
            * docker (container) run ~~~
        * 방법 2
        * ``` FROM nginx```
        * ```COPY nginx.conf /etc/nginx/nginx.conf```
    * 도커 toolbox에 설치 
    * ``` $ docker nginx```
    * 도커 desktop에서는 검색 중 nginx에 넣어둔다

* * * 

### Docker 이미지 조작
1. 이미지 실행하기
    * docker portforwarding을 해야한다. 왜냐하면 virture box를 사용하기 때문이다.
    * docker desktop 사용시에는 필요없다.
    * vm ware에서 설정 > 네트워크 > 고급 > 포트 포워딩 > 다음과 같이 http , https 추가하기
    * ``` docker run --name webserver -d -p 80:80 nginx```
    * docker toolbox에 위와 같이 입력
    * 도커에서 내부적으로 쓰는 identifier을 생성해준다.
    - ps를 하면 위와 같이 설치 내역을 볼 수 있는데, docker ps -a로 치면 설치한 것들 다 나온다.
    - 컨테이너 명 미설정시 : 교수님은 elegant_yalow 나는 peaceful_gagarin으로 출력된다 다 다르다
    - 로컬호스트 접속시 해당 페이지 오픈
    - 80:80으로 run중이라서 이렇게 뜨고 있다.

2. 도커 컨테이너 확인
    * 실행중인 container확인  
        - docker ps
        - docker ps -a
    * exited된 컨테이너 삭제
        - docker rm container-name
        - docker rm helloworld
        - docker ps -a
    * container 명 변경 
        - docker rename container -name container-rename
    * container 중지
        - docker stop container web
        - docker stop web
    * container 실행
        - docker start web
    
    * __+alpha__
    * __도커와 가상화와 차이점__
    - [설명 blog](https://medium.com/@darkrasid/docker%EC%99%80-vm-d95d60e56fdd)
    - 시험 삘
* * * 

## DOCKERFILE을 사용한 코드에 의한 서버 구축

1. 도커 contos설치
    * ubuntu, centos:7 image download
    - ```docker pull ubuntu```
    - ```docker pull centos:7 #centos:version(tag)   ```

    * image 상세보기 - __image config Dockerfile 조회__
    - ```docker image inspect ubuntu```

    * image 삭제 - __image 사용하는 container가 있는 경우 사용하는 container삭제후 사용 가능하다. 
    - ```docker image rm imageID ```

    * 사용하지 않는 image삭제
    - ```docker image prune```
    
    * ubuntu, centos:7 container 생성/실행(create/start)
    * centos의 /bin/cal --calendar 실행 -it:표준입출력 사용
    - ``` docker run -it --name centos_cal centos:7 /bin/cal ```
    - ``` docker ps -a```
    - ``` docker run -it --name centos_shell centos:7 bin/bash ```
    - ``` root@:/#adduser test1 //test1 user 생성```
    - ``` root@:/#su test       //test1 user로 변경```
    - ``` testQ:/$ ls -al       //목록보기 ```
    - ``` testQ:/$ exit         // root로 가기 ```
    - ``` test@:/# exit         // docker로 가기 ```

    - ``` docker pa -a          // centos_shell은 exited상태```
    - ``` root@:/#              // ctrl+p+q docker로 가기```
    - ``` docker attach centos_shell``` // root로 
    - ``` root@:/#```

    - ctrl+p+q 누르면 ```root@ef00077585d2:/# read escape sequence```이렇게 출력되고 나오게 만든다
    * 도커를 이용하면 attach를 이용해 이거저거 쓰기 좋다.

    * apt-get -> pip과 같은 기능을 한다. 설치할 때 쓰인다.
    - __앞으로는 운영 및 시스템도 코딩해야한다.__ 위와 같이 `기호를 활용하여 두개의 동작을 한꺼번에 같이 진행할 수 있다.

* * *
### centos
* centos는 ubuntu와 다른 문법 사용한다. but 뿌리는 리눅스이다.
    - 여기서는 나갈려면 :wq : 저장하고 나가기 하기
    * __vi editor__   
    - 해당 리눅스 파일과 디렉토리는 한번 공부할 가치가 있다.
    * chmod
        - changemod : 접근 권한을 바꾸려면 chmod 명령을 사용
        - set u id가 셋팅된 파일부터 공격하려고 한다.
        - 기초 단계에서 따라서 find를 많이 쓴다. --help해서 한번 봐라

### docker 이미지 생성
* 도커의 설정값 등은 이미지라고 한다.
    * 컨테이너로부터 이미지 작성
    - ubuntu, centos 등이 컨테이너입니다. 

#### 정규표현식 
- 정규표현식은 직관적이고 대부분의 언어가 위를 바탕으로 구성되었다.
    - [정규표현식 개념](http://www.nextree.co.kr/p4327/)
    - [정규표현식 test](https://regexr.com/)


## DOCKERfile을 사용한 코드에 의한 서버 구축
### dockerfile을 사용한 구성 관리
1. dockerfile이란?
    *  docker상에서 작동시킬 컨테이너의 구성 정보를 기술하기 위한 파일
        - 강사님이 주신 js파일과 dockerfile 활용
***
## 07/20 
# docker & kubernetest


### yaml파일을 작성하여 kubernetes에 넣어주기

* hifrodo 컨테이너 google cloud에 업로드해주기
    1. google cloud의 클러스터 작성하기
    2. pod.yaml파일 하나 만들어주기
        * 여기서 pod.yaml이란 yaml은 쿠버네틱스 형식, pod는 docker 컨테이너 내 pod(*파드 는 쿠버네티스 애플리케이션의 기본 실행 단위이다. 쿠버네티스 객체 모델 중 만들고 배포할 수 있는 가장 작고 간단한 단위이다. 파드는 클러스터 에서의 Running 프로세스를 나타낸다.*)
        * <pre><code>
        apiVersion: v1
        kind: Pod  # pod 작성
        metadata:
        name: nginx # nginx에 작성
        labels:
            name: nginx
        spec:
        containers:
        - name: nginx
            image: hifrodo/nginx:v1 # image는 hifrodo이고, nginx이미지내 작성
            ports:
            - containerPort: 80
        </code></pre>
    3. 클러스터 터미널에 yaml파일 드래그 앤 드롭하기
    4. 다음의 코드 작성
        * ![image](/uploads/eeab916c654c1dc3767178af010e88ec/image.png)
        <pre><code>
        kubectl create -f pod.yaml
        kubectl get pod -o wide
        </code></pre>
        - 러닝중인지 확인하기
    5. 콘솔에 실행하기
        * nginx의 bash실행하기
        <pre><code>
        kubectl exec -it nginx bash
        </code></pre>
        - bash 들어가서 ls cat등 리눅스 명령어 연습하기
    6. port forward하기
        * ![image](/uploads/fd09f6047856f49e74c3296de74aa277/image.png)
        - ![image](/uploads/9ceb865719d22dc3fd922abbc68ce2c5/image.png)
        ```kubectl delete all --all```로 삭제하기

